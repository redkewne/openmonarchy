import random

from pony.orm import delete, db_session, select
from app.models.models import User, Course, Student, Report, CourseForm


class Factory:

    default_password = 'password'

    @classmethod
    def setup(cls):
        cls.create_users()
        cls.create_courses()
        cls.create_students()

        # finally, add reports for every course in student

    @classmethod
    @db_session
    def teardown(cls):
        delete(s for s in Student)
        delete(c for c in Course)
        delete(u for u in User)

    @classmethod
    @db_session
    def create_users(cls):
        for teacher in ['melike', 'johnny', 'carlos', 'nick']:
            new_user = User(username=teacher)
            new_user.set_password(cls.default_password)

    @classmethod
    @db_session
    def create_courses(cls):

        for course, year in {'ict': 9,
                             'math': 8,
                             'english': 9,
                             'albanian': 10,
                             'economics': 10,
                             'science': 8}.items():

            teacher = [u for u in select(u for u in User).random(1)]

            Course(name=course.upper(), year=year, teacher=teacher[0])

    @classmethod
    @db_session
    def create_students(cls):
        year_age_chart = {8: 13, 9: 14, 10: 15}

        for i in range(60):
            student_name = 'test student' + str(i)
            student_year = random.choice([i for i in range(8, 11)])

            new_student = Student(name=student_name, age=year_age_chart[student_year], year=student_year)

            course = [c for c in select(c for c in Course if c.year == student_year).random(1)]
            new_student.courses.add(course)

            course = [c for c in select(c for c in Course if c.year == student_year).random(1)]
            new_student.courses.add(course)

            new_student.report = Report(student=new_student)
            for course in [course for course in new_student.courses.select()]:
                CourseForm(course=course, associated_report=new_student.report)
