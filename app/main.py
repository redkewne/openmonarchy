from socket import gethostname
from app import app
from views.views import index


if __name__ == '__main__':
    if 'liveconsole' not in gethostname():
        app.run()
