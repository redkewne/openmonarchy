 $(document).ready(function(){
    $('select').formSelect();
    $('.tooltipped').tooltip();
    $('#header').fadeIn(250, function() {loadLogin()});
    }
)

function loadLogin(){

    children1 = ['#lform', '#login_username', '#login_password']
    children2 = ['#login_buttons', '#login_info']

    for (let i = 0; i < children1.length; i++) {
        $(children1[i]).fadeIn(250, function() {
            for (let i = 0; i < children2.length; i++) {
                $(children2[i]).fadeIn(250)
                }
            }
        )
    }
}

function hit(endpoint){
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
     var res = this;
     $('#mainBody').animate({'opacity':'0.0'}, 300, function() {
     document.getElementById("mainBody").innerHTML = res.responseText;})
     $('#mainBody').animate({'opacity':'1'}, 300);
    }
  };
  var URL = Flask.url_for(endpoint);
  xhttp.open("GET", URL, true);
  xhttp.send();
}

function get_student(student){
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
     var res = this;
     $('#mainBody').animate({'opacity':'0.0'}, 300, function() {
     document.getElementById("mainBody").innerHTML = res.responseText;})
     $('#mainBody').animate({'opacity':'1'}, 300);
    }
  };
  var URL = Flask.url_for('course_survey', {student: student});
  xhttp.open("GET", URL, true);
  xhttp.send();
}

function get_students_in_course(course, year){
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
     var res = this;
     $('#mainBody').animate({'opacity':'0.0'}, 300, function() {
     document.getElementById("mainBody").innerHTML = res.responseText;})
     $('#mainBody').animate({'opacity':'1'}, 300);
    }
  };
  var URL = Flask.url_for('get_students_in_course', {course: course, year: year});
  xhttp.open("GET", URL, true);
  xhttp.send();
}

function view_report_template(course, year){
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
     var res = this;
     $('#mainBody').animate({'opacity':'0.0'}, 300, function() {
     document.getElementById("mainBody").innerHTML = res.responseText;})
     $('#mainBody').animate({'opacity':'1'}, 300);
    }
  };
  var URL = Flask.url_for('report_template', {course: course, year: year});
  xhttp.open("GET", URL, true);
  xhttp.send();
}

function submitReportTemplate(course, year){
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
        var res = this;
        $('#mainBody').animate({'opacity':'0.0'}, 300, function() {
        document.getElementById("mainBody").innerHTML = '<h5>Report template saved!</h5>';})
        $('#mainBody').animate({'opacity':'1'}, 300, function() {

        $('#mainBody').animate({'opacity':'0.0'}, 600, function() {
        document.getElementById("mainBody").innerHTML = res.responseText;})
        $('#mainBody').animate({'opacity':'1'}, 300);});}
    }
  form_elements = document.getElementById("report_template");
  responses = [];

  for (i = 0; i < form_elements.length; i++) {
    responses.push(form_elements[i].value);
  }
  var URL = Flask.url_for('report_template', {course: course, year: year})
  xhttp.open("POST", URL, true);
  xhttp.send(JSON.stringify(responses));
}

function select_courses(){
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
        var res = this;
        $('#mainBody').animate({'opacity':'0.0'}, 300, function() {
        document.getElementById("mainBody").innerHTML = res.responseText;})
        $('#mainBody').animate({'opacity':'1'}, 300);}
    }
    var URL = Flask.url_for('select_courses');
    var pairs = $('#selectcoursesform').serialize().slice(0).split('&');
    var result = {};
    pairs.forEach(function(pair) {
        pair = pair.split('=');
        result[pair[0]] = decodeURIComponent(pair[1] || '');
    });
    xhttp.open("POST", URL, true);
    xhttp.send(JSON.stringify(result));
  }

function select_student_courses(student_name, year){
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
        var res = this;
        $('#mainBody').animate({'opacity':'0.0'}, 300, function() {
        document.getElementById("mainBody").innerHTML = '<h5>Student enrolled in course(s)!</h5>';})
        $('#mainBody').animate({'opacity':'1'}, 300, function() {

        $('#mainBody').animate({'opacity':'0.0'}, 600, function() {
        document.getElementById("mainBody").innerHTML = res.responseText;})
        $('#mainBody').animate({'opacity':'1'}, 300);});}
    }
    var URL = Flask.url_for('select_student_courses', {student_name: student_name, year: year});
    var pairs = $('#selectstudentcourses').serialize().slice(0).split('&');
    var result = {};
    pairs.forEach(function(pair) {
        pair = pair.split('=');
        result[pair[0]] = decodeURIComponent(pair[1] || '');
    });
    xhttp.open("POST", URL, true);
    xhttp.send(JSON.stringify(result));
  }

function submitSurvey(){
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
        var res = this;
        $('#mainBody').animate({'opacity':'0.0'}, 300, function() {
        document.getElementById("mainBody").innerHTML = '<h5>Form saved!</h5>';})
        $('#mainBody').animate({'opacity':'1'}, 300, function() {

        $('#mainBody').animate({'opacity':'0.0'}, 600, function() {
        document.getElementById("mainBody").innerHTML = res.responseText;})
        $('#mainBody').animate({'opacity':'1'}, 300);});}
    }
    var URL = Flask.url_for('course_survey')
    var pairs = $('#surveyform').serialize().slice(0).split('&');
    var result = {};
    pairs.forEach(function(pair) {
        pair = pair.split('=');
        result[pair[0]] = decodeURIComponent(pair[1] || '');
    });
    xhttp.open("POST", URL, true);
    xhttp.send(JSON.stringify(result));
  }
