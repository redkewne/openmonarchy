from flask import Flask
from flask_login import LoginManager
from flask_jsglue import JSGlue
from pony.orm import Database
from keygen import generate

app = Flask(__name__)

app.secret_key = generate()

login = LoginManager(app)
login.login_view = 'login'

jsglue = JSGlue(app)
db = Database()
