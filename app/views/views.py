from flask import render_template, request, redirect, url_for, jsonify
from flask_login import current_user, login_user, login_required, logout_user
from pony.orm import db_session, select

from app import app
from controller import controller
from models.models import User, Student, Course, CourseForm, Report
from forms import LoginForm


admins = ('johnny', 'xhensila', 'paul')

@app.route('/login', methods=['GET', 'POST'])
@db_session
def login():

    if current_user.is_authenticated:
        return redirect(url_for('index'))

    form = LoginForm()

    if form.validate_on_submit():
        user = User.get(username=form.username.data)

        if user is None or not user.check_password(form.password.data):
            return redirect(url_for('login'))

        login_user(user)  # remember=form.remember_me.data)
        return redirect(url_for('index'))

    return render_template('login.html', title='Sign In', form=form)


@app.route('/logout')
@login_required
def logout():
    logout_user()
    return redirect('/')


@app.route('/admin')
@login_required
def admin():
    return render_template('admin.html')

@app.route('/')
@login_required
def index():
    if current_user.username in admins:
        admin_button = 'visible'

    else:
        admin_button = 'hidden'

    return render_template('index.html', ADMIN_BUTTON=admin_button)


@app.route('/entrypoint')
@login_required
def entrypoint():
    return render_template('entrypoint.html')


@app.route('/report_template', methods=['GET', 'POST'])
@db_session
@login_required
def report_template():

    course, year = request.args.get('course'), request.args.get('year')

    if request.method == 'POST':
        controller.set_report_subjects(course, year, request.get_json(force=True))
        return redirect(url_for('list_courses'))

    else:
        subjects = controller.get_report_subjects(course, year)
        return render_template('report_template.html', subjects=subjects, course=course, year=year)


@app.route('/course_survey', methods=['GET', 'POST'])
@db_session
@login_required
def course_survey():

    tutor_table_data = ("Attends class punctually and has equipment ready to use.",
                        "Is actively engaged in all aspects of the lesson.",
                        "Behaves appropriately and considerately in class.",
                        "Demonstrates an ability to sustain concentration.",
                        "Uses class time to complete work effectively.",
                        "Completes assignments and meets deadlines.")

    if request.method == 'POST':
        data = request.get_json(force=True)
        controller.save_report_form(data, current_user)

        return redirect(url_for('list_students'))

    else:
        student = Student.get(name=request.args.get('student'))
        data = controller.get_student_report_data(student, current_user)

        return render_template('report_survey.html', data=data,
                               tutor_table_data=tutor_table_data)

@app.route('/edit_student_info/<student_name>', methods=['GET', 'POST'])
@db_session
@login_required
def edit_student_info(student_name):

    if request.method == 'POST':
        data = request.form.to_dict()
        student = Student.get(id=data['id'])

        if student.name != data['name']:
            student.name = data['name']

        return redirect(url_for('list_students'))

    elif request.method == 'GET':
        student = Student.get(name=student_name)

        return render_template('edit_student.html',
                               ID=student.id, NAME=student.name, YEAR=student.year)

@app.route('/new_user', methods=['GET', 'POST'])
@db_session
def new_user_form():
    if request.method == 'GET':
        return render_template('newuserform.html')

    elif request.method == 'POST':
        data = request.form.to_dict()

        u = User(username=data['username'])
        u.set_password(data['password'])

        return 'User {} created.'.format(u.username)


@app.route('/create_student', methods=['GET', 'POST'])
@db_session
@login_required
def create_student():

    if request.method == 'POST':
        data = request.form.to_dict()
        student = Student(name=data['name'], year=data['year'])

        return redirect(url_for('select_student_courses',
                                student_name=student.name, year=student.year))

    elif request.method == 'GET':
        return render_template('create_student.html')


@app.route('/select_student_courses/<student_name>/<year>', methods=['GET', 'POST'])
@db_session
@login_required
def select_student_courses(student_name, year):

    if request.method == 'GET':
        student = Student.get(name=student_name)
        available_courses = list(select(course.name for course in Course if course.year == year
                                        and course not in student.courses))

        return render_template('student_courses.html',
                               COURSES=available_courses, STUDENT=student_name, YEAR=year)

    else:
        data = request.get_json(force=True)
        student = Student.get(name=student_name)
        if not student.report:
            student.report = Report(student=student)
        for course, condition in data.items():

            course_name = course.replace("%20", " ")
            course_year = ''.join((char if char in '0123456789' else '') for char in course_name)
            course_name = course_name.replace(course_year, '')

            course = Course.get(name=course_name, year=course_year)

            if course:
                student.courses.add(course)
                CourseForm(course=course, associated_report=student.report)

        return render_template('successfully_added.html')

@app.route('/list_courses')
@login_required
def list_courses():

    if current_user.username in admins:
        data = controller.get_all_courses()
        print(data)

    else:
        data = controller.get_my_courses(current_user)

    if not data:
        return render_template('no_courses.html')

    return render_template('list_courses.html', data=data)

@app.route('/list_teachers')
@db_session
@login_required
def list_teachers():

    if current_user.username in admins:
        teachers = select(u for u in User)
        data = [[teacher.username,
                teacher.courses.count(),
                 sum([course.students.count() for course in teacher.courses])]
                for teacher in teachers]

        return render_template('list_teachers.html', data=data)


@app.route('/edit_teacher_info/<user_name>', methods=['GET', 'POST'])
@db_session
@login_required
def edit_teacher_info(user_name):

    if current_user.username in admins:

        if request.method == 'POST':
            data = request.form.to_dict()
            user = User.get(id=data['id'])

            if user.username != data['name']:
                user.username = data['name']

            if data['password'] != '':
                user.set_password(data['password'])

            return redirect(url_for('list_teachers'))

        elif request.method == 'GET':
            user = User.get(username=user_name)

            return render_template('edit_teacher.html',
                                   ID=user.id, NAME=user.username)

    else:
        return redirect('/')

@app.route('/remove_from_course/<course>/<year>')
@db_session
@login_required
def remove_from_course(course, year):
    if current_user.username in admins:
        c = Course.get(name=course, year=year)
        c.teacher = None
        return redirect(url_for('list_courses'))
    else:
        return redirect('/')

@app.route('/list_students')
@db_session
@login_required
def list_students():

    if current_user.username in admins:
        data, count = controller.get_all_students()

    else:
        data, count = controller.get_my_students(current_user)

    visibility = 'hidden'
    if current_user.username in admins:
        visibility = 'visible'

    return render_template('list_students.html', data=data,
                           count=count, vis=visibility)


@app.route('/get_students_in_course')
@login_required
def get_students_in_course():
    course = request.args.get('course')
    year = request.args.get('year')

    data = controller.get_students_in_course(course, year, current_user)

    return render_template('list_students.html', data=data, course=course, year=year)


@app.route('/select_courses', methods=['GET', 'POST'])
@db_session
@login_required
def select_courses():
    if request.method == 'GET':
        data = controller.get_available_courses()

        return render_template('select_courses.html', data=data)

    else:
        data = request.get_json(force=True)
        controller.add_selected_courses(data, current_user)

        return redirect(url_for('list_courses'))

@app.route('/reset')
@db_session
@login_required
def reset():
    if current_user.username == 'johnny':
        for s in select(s for s in Student):
            s.report.tutor_comment = ''
            for cf in s.report.course_forms:
                cf.comment = ''
                cf.grade = 0
                cf.answers = ['Undecided' for i in range(7)]

    return "Student forms cleared."

# @db_session
# def get_info_from(student):
#
#     return [
#         [cf.course.name,
#          [
#              {'comment': cf.comment,
#               'grade': cf.grade,
#               'answers': [[cf.course.objectives[i], cf.answers[i]] for i in range(7)],
#               'objectives': cf.course.objectives}
#          ]]
#         for cf in student.report.course_forms]


# @db_session
# def get_student_info(student):
#
#     return {'name': student.name, 'year': student.year,
#             'tutor_comment': student.report.tutor_comment,
#             'forms': get_info_from(student)}


# @app.route('/preview', methods=['GET', 'POST'])
# @db_session
# def get_all_student_info():
#
#     data = [get_student_info(student) for student in Student.select()]
#
# #    docreader.export_data(data[0])
#
#     return data[0]['name']
# #    return jsonify(data)


# @app.route('/final', methods=['GET', 'POST'])
# @db_session
# def final_page():
#
#     data = [get_student_info(student) for student in Student.select()]
#
#     docreader.generate_final_page(data[0])
#
#     return data[0]['name']
#
#     return jsonify(data[0])


# @app.route('/access')
# @db_session
# def access_word_doc():
#
#     data = docreader.get_main()
#
#     return jsonify(data)

# @db_session
# def change_password():
#     password = '01010101'
#     user = current_user.username
#     change = user.set_password(password)
#     pass
#
#
# @db_session
# def get_grades(teacher):
#
#     data = [{'teacher': teacher.username,
#              'course.name': course_form.course.name,
#              'grade': course_form.course.grade}
#             for course_form in
#             [course_form for course_form in [course.course_surveys for course in teacher.courses]]]
#
#     return data
#
#
# @app.route('/grades', methods=['GET', 'POST'])
# @db_session
# def view_grades_by_teacher():
#     data = []
#
#     for teacher in User.select():
#         data.append(get_grades(teacher))
#
#     """    for teacher in User.select():
#
#             for course in teacher.courses:
#                 for course_form in course.course_surveys:
#                     data.append({'teacher': teacher.username,
#                                  'course': course.name,
#                                  'grade': course_form.grade}) """
#     print(data)
#     return jsonify(data)
#
#
# @app.route('/progress', methods=['GET', 'POST'])
# @db_session
# def view_students_and_teachers():
#
#     data = []
#
#     for student in Student.select():
#         data.append({'student': student.name,
#                     'teachers': [t.username for t in student.teachers.select()]})
#
#     return jsonify(data)


def add_student():
    pass


def edit_student():
    pass


def remove_student():
    pass


def add_course_to_student():
    pass


def remove_course_from_student():
    pass


def add_course_to_teacher():
    pass


def remove_course_from_teacher():
    pass


@app.route('/review')
@db_session
@login_required
def review_mock_data():
    # Code aesthetics example: dense vs sparse
    # data = [{teacher.username: [{course.name: [student.name for student in course.students.select()]}
    #                            for course in teacher.courses.select()]} for teacher in User.select()]

    data = [
        {teacher.username: [
                {'{} Y{}'.format(course.name, course.year): [student.name for student in course.students.select()]}
                for course in teacher.courses.select()]}
        for teacher in User.select()]

    return jsonify(data)

# Endpoints below are currently for development and debugging purposes! #


"""
@app.route('/add_all')
@db_session
def add_all():
    students_add.add_all()

    return "All students added successfully"


@app.route('/assign_all/<limit>')
@db_session
def assign_all(limit):
    res = controller.assign_all_courses(limit)

    return res
"""
