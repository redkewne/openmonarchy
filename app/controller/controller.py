import json
from models.models import User, CourseForm, Course, Student
from pony.orm import db_session, select


@db_session
def get_teacher_courses(user):
    courses = [[course.name, course.year]
               for course in select(c for c in Course if user.username == c.teacher)]
    return courses


@db_session
def get_students_in_course(course, year, user):
    students = [[student.name,
                 student.year,
                 student.courses.filter(lambda c: c.teacher.username == user.username).count(),
                 student.courses.count()]
                for student in Course.get(name=course,
                                          year=year).students.select()]
    return students


@db_session
def get_my_courses(user):
    course_data = [[course.name, course.year, course.students.count(), course.teacher.username]
                   for course in select(c for c in Course if c.teacher.username == user.username)]

    return course_data


@db_session
def get_all_courses():
    course_data = []
    for course in select(c for c in Course):
        name = course.name
        year = course.year
        students = course.students.count()
        teacher = 'None'
        if course.teacher:
            teacher = course.teacher.username
        course_data.append([name, year, students, teacher])

    return course_data


@db_session
def get_my_students(user):
    students = []

    for course in User.get(username=user.username).courses.select():
        for student in course.students.select():

            common_course_count = student.courses.filter(lambda c: c.teacher.username == user.username).count()
            student_data = [student.name, student.year, common_course_count, student.courses.count()]

            if student_data not in students:
                students.append(student_data)

    return students, len(students)


@db_session
def get_all_students():

    students = []
    query = select(s for s in Student)

    for student in list(query):

        common_course_count = 0
        student_data = [student.name, student.year, common_course_count, student.courses.count()]

        if student_data not in students:
            students.append(student_data)

    return students, len(students)


@db_session
def get_report_subjects(course, year):
    subjects = Course.get(name=course, year=year).objectives

    return subjects


@db_session
def set_report_subjects(course, year, subjects):
    Course.get(name=course, year=year).objectives = subjects


@db_session
def save_report_form(data, user):
    courses = [course for course in Student.get(name=data['student_name']).courses
               if course.teacher == user]

    tutor_table_choices = [data['tutortable' + str(i)] for i in range(1, 7)]

    student = Student.get(name=data['student_name'])
    fields = {'tutcomment': data['tutcomment'],
              'attendance': data['attendance'],
              'progress': data['progress'],
              'sanctions': data['sanctions'],
              'punctuality': data['punctuality'],
              'table': tutor_table_choices}

    student.report.tutor_comment = json.dumps(fields)

    for course in courses:
        fake_name = course.name.replace(" ", "%20")
        answers = [data[fake_name + str(i + 1)] for i in range(7)]

        for i in range(7):
            answers.extend([data[fake_name + str(i + 1)]])

        course_form = CourseForm.get(associated_report=Student.get(name=data['student_name']).report,
                                     course=course)

        course_form.answers = answers
        course_form.comment = data['comment' + fake_name]
        course_form.grade = data['grade' + fake_name]


@db_session
def get_student_report_data(student, user):

    course_forms = [cf for cf in student.report.course_forms.select()]

    if not student.report.tutor_comment:
        student.report.tutor_comment = '{}'

    tutor_fields = json.loads(student.report.tutor_comment)
    tut_comment = tutor_fields.get('tutcomment', ' ')
    attendance = tutor_fields.get('attendance', '100')
    learning_progress = tutor_fields.get('progress', 'undecided')
    sanctions = tutor_fields.get('sanctions', '0')
    punctuality = tutor_fields.get('punctuality', 'undecided')
    table_answers = tutor_fields.get('table', ['Undecided' for i in range(6)])

    data = {'student': {'name': student.name,
                        'tutor_fields':
                            {'tutcomment': tut_comment,
                             'attendance': attendance,
                             'progress': learning_progress,
                             'sanctions': sanctions,
                             'punctuality': punctuality,
                             'table': table_answers},
                        'courses': [
                            {'name': cf.course.name,
                             'year': cf.course.year,
                             'objectives': cf.course.objectives,
                             'answers': cf.answers,
                             'grade': cf.grade,
                             'comment': cf.comment} for cf in course_forms]}}

    return data


@db_session
def get_available_courses():
    data = [[c.name, c.year] for c in Course.select(lambda c: not c.teacher)]
    years = {}

    for i in range(1, 13):
        years[i] = []

    for course in data:
        years[course[1]].append(course[0])

    return years


@db_session
def add_selected_courses(data, user):
    for course, condition in data.items():

        course_name = course.replace("%20", " ")
        course_year = ''.join((char if char in '0123456789' else '') for char in course_name)
        course_name = course_name.replace(course_year, '')

        course = Course.get(name=course_name, year=course_year)

        if course:
            user.courses.add(course)


@db_session
def assign_all_courses(limit):
    teacher = User.get(username="johnny")

    if limit == 'true':
        course = Course.get(name='Economics', year=9)
        teacher.courses.add(course)

        return "Economics course added to Johnny"

    else:
        for course in Course.select():
            teacher.courses.add(course)

        return "All courses added to Johnny"
