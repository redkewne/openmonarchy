from pony.orm import Required, Optional, Set, Json, composite_key, db_session
from flask_login import UserMixin
from werkzeug.security import generate_password_hash, check_password_hash
from app import db, login


class User(UserMixin, db.Entity):

    username = Required(str, unique=True)
    password_hash = Optional(str)
    courses = Set('Course', reverse='teacher')
    students = Set('Student', reverse='teachers')

    @db_session
    def set_password(self, password):
        self.password_hash = generate_password_hash(password)

    @db_session
    def check_password(self, password):
        return check_password_hash(self.password_hash, password)

    def __repr__(self):
        return '<User {}>'.format(self.username)


@login.user_loader
@db_session
def load_user(id):
    return User.get(id=int(id))


class Course(db.Entity):
    name = Required(str)
    year = Required(int)
    teacher = Optional('User', reverse='courses')
    students = Set('Student', reverse='courses')
    objectives = Optional(Json, default=['' for i in range(7)])
    course_surveys = Set('CourseForm', reverse='course')
    composite_key(name, year)


class Student(db.Entity):
    name = Required(str, unique=True)
    age = Optional(int, default=0)
    courses = Set('Course', reverse='students')
    report = Optional('Report', cascade_delete=True)
    year = Required(int)
    teachers = Set('User', reverse='students')


class Report(db.Entity):
    student = Required('Student', unique=True)
    tutor_comment = Optional(str)
    course_forms = Set('CourseForm', reverse='associated_report')


class CourseForm(db.Entity):
    course = Required('Course', reverse='course_surveys')
    comment = Optional(str, default='')
    grade = Optional(int, default=0)
    answers = Optional(Json, default=['Undecided' for i in range(7)])
    associated_report = Required('Report', reverse='course_forms')


db.bind(provider='sqlite', filename='kingscrmdb', create_db=True)
db.generate_mapping(create_tables=True)
