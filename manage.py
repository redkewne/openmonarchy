""" Helper module used to get system-wide information. """

import sys, os
from factory.factory import Factory


def tests():
    """ Run tests """
    print('Testing.')

def hello():
    """ Acknowledge existence """
    print('Hello!')

def run_linter():
    os.system("pylint app --disable=missing-docstring")

def factory_setup():
    Factory.setup()
    print('Factory setup completed successfully.')

def factory_teardown():
    Factory.teardown()
    print('Factory teardown completed successfully.')

def manage():
    """ Manage system-wide functionality """

    commands = {'tests': tests,
                'hello': hello,
                'lint': run_linter,
                'setup': factory_setup,
                'teardown': factory_teardown}


    if len(sys.argv) == 2:
        commands[sys.argv[1]]()


if __name__ == '__main__':
    manage()
